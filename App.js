import { NavigationContainer } from "@react-navigation/native";
import ProductsContextProvider from "./store/products-context";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AllProductsScreen from "./screens/AllProductsScreen";
import ProductDetailsSceen from "./screens/ProductDetailsScreen";
import CartScreen from "./screens/CartScreen";
import CartContextProvider from "./store/cart-context";
import ShoppingCartIcon from "./components/ShoppingCartIcon";

const App = () => {
    const Stack = createNativeStackNavigator();
    return (
      <ProductsContextProvider>
        <CartContextProvider>
          <NavigationContainer>
            <Stack.Navigator>
              <Stack.Screen
                name="SHOP | All Products"
                component={AllProductsScreen}
                options={{
                  headerRight: () => {
                    return <ShoppingCartIcon />;
                  },
                }}
              />
              <Stack.Screen
                name="Product Details"
                component={ProductDetailsSceen}
                options={{
                  headerRight: () => {
                    return <ShoppingCartIcon />;
                  },
                }}
              />
              <Stack.Screen
                name="Shopping Cart"
                component={CartScreen}
                options={{
                  headerRight: () => {
                    return <ShoppingCartIcon />;
                  },
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </CartContextProvider>
      </ProductsContextProvider>
    );    
}

export default App;
