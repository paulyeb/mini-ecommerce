import { View, Text, Image, Pressable, StyleSheet, Platform } from "react-native";
import { React } from "react";
import { useNavigation } from "@react-navigation/native";

const ProductItem = ({image, description, title, price, id}) => {
    const navigation = useNavigation();

    const pressHandler = () => {
        navigation.navigate('Product Details', {productId: id});
    };

    return <View style={styles.productItem}>
        <Pressable 
            android_ripple={{ color: '#ccc'}}
            style={({pressed}) => (pressed ? styles.buttonPressed : null)}
            onPress={pressHandler}
        >
            <View style = {styles.innerContainer}>
                <View>
                    <Image source={image} style={styles.image} />
                    <View style={styles.imageData}>
                        <Text style={styles.title}>{title}</Text>
                        <Text style={styles.title}>GHC{price}</Text>
                    </View>
                    <Text style={styles.description}>{description}</Text>
                </View>
            </View>
        </Pressable>
    </View>
}

export default ProductItem;

const styles = StyleSheet.create({
    productItem:{
        marginVertical: 20,
        marginHorizontal: 16,
        borderRadius: 8,
        overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
        backgroundColor: 'white',
        shadowOpacity: 0.15,
        shadowOffset: { width: 0, height: 4},
        shadowRadius: 8,
        elevation: 4
    },
    buttonPressed: {
        opacity: 0.5
    },
    innerContainer: {
        borderRadius: 8,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: 200,
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
        margin: 8,
        color: 'red'
    },
    imageData: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    description: {
        padding: 5,
        fontWeight: "bold"
    }
})