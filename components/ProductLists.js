import { useContext } from "react";
import { View } from "react-native";
import { ProductsContext } from "../store/products-context";
import ProductItem from "./ProductItem";

const ProductsList = () => {
    const products = useContext(ProductsContext);

    return <View>
        {products.map(product => <ProductItem key={product.id} id={product.id} title={product.name} image={product.image} price={product.price} description={product.description} bonuses={product.bonuses} provider={product.provider} location={product.location} />)}
    </View>
}

export default ProductsList;