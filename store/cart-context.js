import React, {createContext, useState} from 'react';
import { getProduct } from './products-context';

export const CartContext = createContext();

const CartContextProvider = ({children}) => {
  const [items, setItems] = useState([]);

  function addItemToCart(id) {
    const product = getProduct(id);
    setItems((prevItems) => {
      const item = prevItems.find((item) => (item.id == id));
      if(!item) {
          return [...prevItems, {
              id,
              qty: 1,
              product,
              totalPrice: product.price 
          }];
      }
      else { 
          return prevItems.map((item) => {
            if(item.id == id) {
              item.qty++;
              item.totalPrice += product.price;
            }
            return item;
          });
      }
    });
}

const increaseQuantity = (id) => {
    const product = getProduct(id);
    product.qty++;
    console.log(product);
}

const decreaseQuantity = (id) => {
    const product = getProduct(id);
    console.log(product);
    if(product.qty < 1) {
        return
    } else {
        product.qty = product.qty + 1;
    }
}

function getItemsCount() {
      return items.reduce((sum, item) => (sum + item.qty), 0);
  }

  function getTotalPrice() {
      return items.reduce((sum, item) => (sum + item.totalPrice), 0);
  }  

  return (
    <CartContext.Provider 
      value={{items, setItems, getItemsCount, addItemToCart, getTotalPrice, increaseQuantity, decreaseQuantity}}>
      {children}
    </CartContext.Provider>
  );
}

export default CartContextProvider;