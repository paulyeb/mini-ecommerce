/* eslint-disable prettier/prettier */
import { createContext, React } from 'react';

const PRODUCTS = [
  {
    id: 99,
    name: 'ReactProX Headset',
    price: 350,
    image: require('../assets/products/headset.jpg'),
    description:
      'A headset combines a headphone with microphone. Headsets are made with either a single-earpiece (mono) or a double-earpiece (mono to both ears or stereo).',
    bonuses: ['12-Months Warranty, ', 'Free Headset Wipes, ', 'Spare Charger'],
    provider: 'FREDDIES CORNER',
    location: 'TEMA - ACCRA',
    qty: 0,
  },
  {
    id: 100,
    name: "Tourist's Camera",
    price: 440,
    image: require('../assets/products/product-1.jpg'),
    description:
      'A headset combines a headphone with microphone. Headsets are made with either a single-earpiece (mono) or a double-earpiece (mono to both ears or stereo).',
    bonuses: ['12-Months Warranty, ', 'Spare Charger'],
    provider: 'MELCOM',
    location: 'LAPAZ - ACCRA',
    qty: 0
  },
  {
    id: 101,
    name: 'FastLane Toy Car',
    price: 600,
    image: require('../assets/products/toy-car.jpg'),
    description:
      'A model car, or toy car, is a miniature representation of an automobile. Other miniature motor vehicles, such as trucks, buses, or even ATVs, etc. are often included in this general category.',
    bonuses: ['12-Months Warranty, ', '4-Set Spare Tyres, ', '4 Batteries'],
    provider: 'ERATA MOTORS',
    location: 'SPINTEX - ACCRA',
    qty: 0
  },
  {
    id: 102,
    name: 'SweetHome Cupcake',
    price: 20,
    image: require('../assets/products/cupcake.jpg'),
    description:
      'A cupcake (also British English: fairy cake; Hiberno-English: bun; Australian English: fairy cake or patty cake[1]) is a small cake designed to serve one person.',
    bonuses: ['Extra Icing, ', 'Free Mini Juice, '],
    provider: 'BB BAKERY HOUSE',
    location: 'ACHIMOTA MALL - ACCRA',
    qty: 0
  },
];

export function getProduct(id) {
    return PRODUCTS.find((product) => (product.id == id));
}

export const ProductsContext = createContext();

const ProductsContextProvider = ({children}) => {
    return <ProductsContext.Provider value={PRODUCTS}>{children}</ProductsContext.Provider>;
};

export default ProductsContextProvider;
