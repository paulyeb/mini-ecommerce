import { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, Button } from "react-native";
import { CartContext } from "../store/cart-context";

const CartScreen = () => {
    const {items, getItemsCount, getTotalPrice, increaseQuantity, decreaseQuantity} = useContext(CartContext);
    // console.log(items);

    
    let [total, setTotal] = useState(0);
    useEffect(() => {
        setTotal(getTotalPrice())
    }) 
    
    const decrease = (id) => {
        const item = items.find(item => id == item.id)
        // console.log('press', id)
        decreaseQuantity(item.id);
    }

    const increase = (id) => {
        increaseQuantity(id);
    }
   

    return <View style={styles.shoppingCartContainer}>
        {items.map(item => <View style={styles.productData} key={item.id}>
            <Image source={item.product.image} style={styles.image} />
            <View style={styles.detailsContainer}>
                <View style={styles.upperRowData}>
                    <Text>{item.product.name}</Text>
                    <Text>QTY: {item.qty}</Text>
                </View>
                <View style={styles.lowerRowData}>
                    <Text>PRICE: GHC{item.product.price * item.qty}</Text>
                    <View style={styles.buttons}>
                        <View style={styles.button}>
                            <Button title="-" onPress={() => decrease(item.product.id)} />
                        </View>
                        <View style={styles.button}>
                            <Button title="+" onPress={() => increase(item.product.id)} />
                        </View>
                    </View>
                </View>
            </View>
        </View>)}
        <View style={styles.checkout}>
            <Button title={`CHECKOUT (GHC${total})`} />
        </View>
    </View>
}

export default CartScreen;

const styles = StyleSheet.create({
    shoppingCartContainer: {
        margin: 16,
        height: 100,
    },
    productData: {
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        backgroundColor: "white",
        elevation: 4,
        paddingHorizontal: 5,
        marginBottom: 15
    },
    image: {
        width: 100,
        height: 100,
        resizeMode: 'center'
    },
    detailsContainer: {
        flex: 1,
        marginTop: 18,
        marginHorizontal: 10
    },
    upperRowData: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    lowerRowData: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    buttons: {
        flexDirection: "row",
    },
    button: {
        width: 30,
        marginHorizontal: 1
    },
    checkout: {
        marginTop: 50
    }
})