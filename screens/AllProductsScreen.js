import ProductsList from "../components/ProductLists";
import { View, ScrollView, StyleSheet } from "react-native";

const AllProductsScreen = () => {
    return <View>
        <ScrollView>
            <ProductsList />
        </ScrollView>
    </View>
}

export default AllProductsScreen;