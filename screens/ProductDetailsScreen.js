import { Text, View, Image, StyleSheet, Button, ScrollView } from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import { useContext } from "react";
import { ProductsContext } from "../store/products-context";
import { CartContext } from "../store/cart-context";

const ProductDetailsSceen = () => {
    const route = useRoute();
    const navigation = useNavigation();
    const productId = route.params.productId;
    const products = useContext(ProductsContext);
    let cart = useContext(CartContext);
    const selectedProduct = products.find(product => product.id == productId);
    const {addItemToCart} = useContext(CartContext);

    const addItem = (id) => {
        const item = products.find(product => product.id == id);
        // console.log(item.name, "added to cart");
        // const addedProduct = {
        //     name: item.name,
        //     image: item.image,
        //     price: item.price,
        //     description: item.description
        // }
        // console.log(addedProduct);
        // cart = [...cart, addedProduct];
        // console.log(cart);
        addItemToCart(item.id);
        navigation.navigate("Shopping Cart");
    } 

    return <ScrollView>

    <View style={styles.productItem}>
        <View style = {styles.innerContainer}>
            <View>
                <Image source={selectedProduct.image} style={styles.image} />
                <View style={styles.imageData}>
                    <Text style={styles.title}>{selectedProduct.name}</Text>
                    <Text style={styles.title}>GHC{selectedProduct.price}</Text>
                </View>
                <Text style={styles.description}>{selectedProduct.description}</Text>
                {selectedProduct.bonuses ? <Text style={styles.description}>Bonuses: {selectedProduct.bonuses}</Text> : null}
                {selectedProduct.provider ? <Text style={styles.description}>Provider: {selectedProduct.provider}</Text> : null}
                {selectedProduct.location ? <Text style={styles.description}>Location: {selectedProduct.location}</Text> : null}
            </View>
            <Button style={styles.button} title="ADD TO CART" onPress={() => addItem(selectedProduct.id)}/>
        </View>
</View>
    </ScrollView> 
}

export default ProductDetailsSceen;

const styles = StyleSheet.create({
    productItem:{
        marginVertical: 20,
        marginHorizontal: 16,
        borderRadius: 8,
        overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
        backgroundColor: 'white',
        shadowOpacity: 0.15,
        shadowOffset: { width: 0, height: 4},
        shadowRadius: 8,
        elevation: 4
    },
    innerContainer: {
        borderRadius: 8,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: 250,
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
        margin: 8,
        color: 'red'
    },
    button: {
        width: 20,
        height: 200
    },
    imageData: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    description: {
        padding: 5,
        fontWeight: "bold"
    }
})